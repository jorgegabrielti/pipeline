# Command line instructions
You can also upload existing files from your computer using the instructions below.

### Git global setup
```bash
git config --global user.name "Jorge Gabriel"
git config --global user.email "jorgegabriel.ti@gmail.com"
```

### Create a new repository
```bash
git clone git@gitlab.com:jorgegabrielti/pipeline.git
cd pipeline
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

### Push an existing folder
```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:jorgegabrielti/pipeline.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Push an existing Git repository
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:jorgegabrielti/pipeline.git
git push -u origin --all
git push -u origin --tags
```